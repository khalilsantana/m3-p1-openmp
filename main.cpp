#include <omp.h>
#include <iostream>
#include <fstream>
#include <vector>
#include <chrono>

std::vector<char> readFile(std::string path) {
    std::ifstream file(path, std::ios::binary | std::ios::ate);
    std::streamsize size = file.tellg();
    file.seekg(0, std::ios::beg);
    std::vector<char> buffer(size);
    file.read(buffer.data(), size);
    return buffer;
}

int main(int argc, char *argv[]) {
     using namespace std::chrono;

    // Check if user provided the correct number of args and then proceed, exit otherwise
    if (argc != 4) {
        std::printf("Usage: %s <path-to-input-image>.bmp <path-to-output-image>.bmp <n-threads>\n", argv[0]);
        return EXIT_FAILURE;
    }

    std::string imagePath = argv[1];
    std::string imageOutPath = argv[2];
    int n_threads = std::atoi(argv[3]);
    std::vector<char> filebuf;
    // For some reason, BMPs have their witdh and height as *signed*
    int imageWidth, imageHeight;
    // BMP header ends in ther 54th byte
    const int BMP_HEADER_END = 53;

    filebuf = readFile(imagePath);
    std::printf("Buffer size: %iMB\n", filebuf.size()/(1024*1024));

    // This reads bytes 18-21 for width, and 22-25 for height
    // &filebuf[i] gets a ptr ref, then we cast it to an int ptr (int *)
    // after that, we derefrence this int ptr (*), and store it in imageWidth/Height
    imageWidth = *(int *)&filebuf[18];
    imageHeight = *(int *)&filebuf[22];
    
    std::printf("W: %u, H: %i\n", imageWidth, imageHeight);
    
    high_resolution_clock::time_point t1 = high_resolution_clock::now();
    omp_set_num_threads(n_threads);
    # pragma omp parallel
    {
        
        int id = omp_get_thread_num();

        int length = filebuf.size() - BMP_HEADER_END;
        int block = length / n_threads;
        int start = id * block + BMP_HEADER_END;
        int end = start + block;
        if (id == n_threads - 1) {
            end = length;
        }

        // std::printf("Id: %i, Block %i", id, block);

        for (int i=start; i<end;i+=3) {
            // BMPs are stored in BGR format, not RGB
            uint8_t B = filebuf[i];
            uint8_t G = filebuf[i+1];
            uint8_t R = filebuf[i+2];
            uint8_t L = (B + G + R)/3;

            // Comment or remove this line for non-debug runs, otherwise the blocking IO will *severly* slow down
            // when running this programm with a big image
            // std::printf("I: %i, R: %hhx, G: %hhx, B: %hhx, L: %hhx\n", i, R, G, B, L);

            // Edit the image in the memory
            filebuf[i] = L;
            filebuf[i+1] = L;
            filebuf[i+2] = L;
        }
    }
    high_resolution_clock::time_point t2 = std::chrono::high_resolution_clock::now();
    duration<double> time_span = duration_cast<duration<double>>(t2 - t1);
    std::cout << "It took me " << time_span.count() << " seconds." << std::endl;
    // Write the buffer (image) to disk
    std::ofstream fout(imageOutPath, std::ios::out | std::ios::binary);
    fout.write((char*)&filebuf[0], sizeof(char)* filebuf.size());
    fout.close();
    return EXIT_SUCCESS;
}